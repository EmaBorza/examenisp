package examen_2020_exer2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Main {

    public static void main(String[] args) {
	// write your code here
        new Frame();
    }
}

class Frame extends JFrame {

    public JTextArea ja;
    public JTextField jf;
    public JButton jb;

    Frame() {
        setTitle("Exer2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        init();
        setSize(300,250);
        setVisible(true);
    }

    public void init(){

        jf=new JTextField();
        jf.setBounds(10, 30, 250, 30);
        add(jf);

        ja=new JTextArea();
        ja.setBounds(10, 70, 250, 40);
        add(ja);

        jb = new JButton("Move");
        jb.setBounds(80, 130, 100, 50);
        add(jb);
        jb.addActionListener(new ActionMove());
    }
   class ActionMove implements ActionListener {
        void move()
        {
            try
            {
                ja.setText(jf.getText());
                jf.setText("");

            }catch(Exception e)
            {
            }

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            move();
        }
    }
}
